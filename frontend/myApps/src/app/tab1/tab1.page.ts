import { Component } from '@angular/core';
import { GothService } from '../callapi/goth.service';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  dataUser:any;
  dataUserBk: any;
  valBol:boolean;
  valorr:any;
  constructor(public goth:GothService) {}

  ngOnInit(){
    this.getDataUser();
  }

   getDataUser(){
     this.goth.obtenerDatos().subscribe(res => {
      this.dataUser = res;
      this.dataUserBk = res;
      console.log(res);
    }, err => {
      console.log("Error!", err);
    })
  }

  getDataByID(val){
    this.valBol = !Number(val);
    let valorr = val;
    let data= [];
    let data_filtrada = [];
    console.log(this.valBol);
    console.log(this.valorr);
    console.log(val);
    if(this.valBol){
      console.log("es estring",val.toString()); 
      data_filtrada = this.dataUserBk.filter( (str) => { 
        return str.nombre.toLowerCase().indexOf(val.toString()) !== -1; });
      console.log("datafilt",data_filtrada);
      data.push(data_filtrada);
      this.dataUser = data[0];
      
    }else
    {
      data_filtrada = this.dataUserBk.find(char => char.id == val);
      data.push(data_filtrada);
      this.dataUser = data;
    }
   
    if (val.trim() == "")
    this.dataUser = this.dataUserBk;
    

  }
  
}
