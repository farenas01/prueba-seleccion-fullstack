import { TestBed } from '@angular/core/testing';

import { GothService } from './goth.service';

describe('GothService', () => {
  let service: GothService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GothService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
