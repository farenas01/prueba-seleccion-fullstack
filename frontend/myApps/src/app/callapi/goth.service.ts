import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class GothService {
apiUrl = "http://localhost:3000/characters";
  constructor(public http: HttpClient) { }

  obtenerDatos(){
    return this.http.get(this.apiUrl);
  }

}
