const express = require('express');
const { cargaPersonajes } = require('./cargaPersonajes/cargaPersonajes');
let cargados = [];
try {
    cargados = require('./db/characters.json');
    console.log('Ya tiene data');
} catch (error) {
    cargados = cargaPersonajes();
    console.log('Cargando la data');
}


const app = express()

app.get('/characters', (req, res) => {
    res.send(cargados)
});

app.get("/characters/:id", (req, res) => {
    console.log(req.params.id);
    let filtrados = cargados.find(caracter => caracter.id == req.params.id);
    console.log(filtrados);
    res.send(filtrados)
});

app.listen(3000, () => {
    console.log("Escuchando peticiones en el puerto 3000");
});