const axios = require('axios');
const fs = require('fs');
let listaDeCaracteres = [];

let cargaPersonajes = async() => {
    const instance = axios.create({
        baseURL: 'https://api.got.show/api/general/characters',
    });
    await instance.get().then(resp => {
            for (i = 0; i < resp.data.book.length; i++) {
                let nombre = resp.data.book[i].name;
                let personaje = {
                    id: i,
                    nombre: nombre
                }
                listaDeCaracteres.push(personaje);
            }


        })
        .catch(err => console.log(err));

    let caracteres = guardarDb(listaDeCaracteres)
    return caracteres;

};

let guardarDb = (listaDeCaracteres) => {
    let data = JSON.stringify(listaDeCaracteres);
    fs.writeFile('db/characters.json', data, () => {
        console.log('Archivo creado');
        return data;
    });

}



module.exports = {
    cargaPersonajes
}